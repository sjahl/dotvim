set background=dark
hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "mnmlst"


" Remove all highlighting
highlight clear Constant
highlight clear Number
highlight clear Statement
highlight clear PreProc
highlight clear Type
highlight clear Special
highlight clear Identifier

highlight clear String
highlight clear Comment
highlight clear Error
highlight clear LineNr
highlight clear NonText
highlight clear SpecialKey

hi ErrorMsg         guifg=white       guibg=#FF6C60     gui=BOLD      ctermfg=white       ctermbg=red         cterm=NONE
hi WarningMsg       guifg=white       guibg=#FF6C60     gui=BOLD      ctermfg=white       ctermbg=red         cterm=NONE
hi LineNr           guifg=#3D3D3D     guibg=black       gui=NONE      ctermfg=darkgray    ctermbg=NONE        cterm=NONE
"hi CursorLine       guifg=none       guibg=#282a2e      gui=NONE      ctermfg=NONE        ctermbg=darkgray    cterm=NONE


" Syntax highlighting
hi Comment          guifg=#7C7C7C     guibg=NONE        gui=NONE      ctermfg=darkgray    ctermbg=NONE        cterm=NONE
hi String           guifg=#A8FF60     guibg=NONE        gui=NONE      ctermfg=green       ctermbg=NONE        cterm=NONE
hi StringDelimiter  guifg=#A8FF60     guibg=NONE        gui=NONE      ctermfg=green       ctermbg=NONE        cterm=NONE
"hi Number           guifg=#FF73FD     guibg=NONE        gui=NONE      ctermfg=magenta     ctermbg=NONE        cterm=NONE

hi rubyStringDelimiter         guifg=#A8FF60 guibg=NONE      gui=NONE      ctermfg=green ctermbg=NONE      cterm=NONE
hi shStringDelimiter         guifg=#A8FF60 guibg=NONE      gui=NONE      ctermfg=green ctermbg=NONE      cterm=NONE

