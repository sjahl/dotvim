" Vim color file
" Converted from Textmate theme Sunset using Coloration v0.2.5 (http://github.com/sickill/coloration)

set background=dark
highlight clear

if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "Sunset"

hi Cursor  guifg=NONE guibg=#000000 gui=NONE
hi Visual  guifg=NONE guibg=#c3dcff gui=NONE
hi CursorLine  guifg=NONE guibg=#edead5 gui=NONE
hi CursorColumn  guifg=NONE guibg=#edead5 gui=NONE
hi LineNr  guifg=#807e73 guibg=#fffce5 gui=NONE
hi VertSplit  guifg=#cfccb9 guibg=#cfccb9 gui=NONE
hi MatchParen  guifg=#294277 guibg=NONE gui=NONE
hi StatusLine  guifg=#000000 guibg=#cfccb9 gui=bold
hi StatusLineNC  guifg=#000000 guibg=#cfccb9 gui=NONE
hi Pmenu  guifg=NONE guibg=NONE gui=NONE
hi PmenuSel  guifg=NONE guibg=#c3dcff gui=NONE
hi IncSearch  guifg=NONE guibg=#b8bfc1 gui=NONE
hi Search  guifg=NONE guibg=#b8bfc1 gui=NONE
hi Directory  guifg=#c5060b guibg=NONE gui=NONE
hi Folded  guifg=#c3741c guibg=#fffce5 gui=NONE

hi Normal  guifg=#000000 guibg=#fffce5 gui=NONE
hi Boolean  guifg=#004080 guibg=NONE gui=NONE
hi Character  guifg=#c5060b guibg=NONE gui=NONE
hi Comment  guifg=#c3741c guibg=NONE gui=NONE
hi Conditional  guifg=#294277 guibg=NONE gui=NONE
hi Constant  guifg=#c5060b guibg=NONE gui=NONE
hi Define  guifg=#294277 guibg=NONE gui=NONE
hi ErrorMsg  guifg=#ffffff guibg=#990000 gui=NONE
hi WarningMsg  guifg=#ffffff guibg=#990000 gui=NONE
hi Float  guifg=#294277 guibg=NONE gui=NONE
hi Function  guifg=#476a97 guibg=NONE gui=NONE
hi Identifier  guifg=#294277 guibg=NONE gui=NONE
hi Keyword  guifg=#294277 guibg=NONE gui=NONE
hi Label  guifg=#df0700 guibg=NONE gui=NONE
hi NonText  guifg=#bfbfbf guibg=#edead5 gui=NONE
hi Number  guifg=#294277 guibg=NONE gui=NONE
hi Operator  guifg=#294277 guibg=NONE gui=NONE
hi PreProc  guifg=#294277 guibg=NONE gui=NONE
hi Special  guifg=#000000 guibg=NONE gui=NONE
hi SpecialKey  guifg=#bfbfbf guibg=#edead5 gui=NONE
hi Statement  guifg=#294277 guibg=NONE gui=NONE
hi StorageClass  guifg=#294277 guibg=NONE gui=NONE
hi String  guifg=#df0700 guibg=NONE gui=NONE
hi Tag  guifg=NONE guibg=NONE gui=NONE
hi Title  guifg=#000000 guibg=NONE gui=bold
hi Todo  guifg=#c3741c guibg=NONE gui=inverse,bold
hi Type  guifg=NONE guibg=NONE gui=NONE
hi Underlined  guifg=NONE guibg=NONE gui=underline
hi rubyClass  guifg=#294277 guibg=NONE gui=NONE
hi rubyFunction  guifg=#476a97 guibg=NONE gui=NONE
hi rubyInterpolationDelimiter  guifg=NONE guibg=NONE gui=NONE
hi rubySymbol  guifg=#c5060b guibg=NONE gui=NONE
hi rubyConstant  guifg=#d0533b guibg=NONE gui=NONE
hi rubyStringDelimiter  guifg=#df0700 guibg=NONE gui=NONE
hi rubyBlockParameter  guifg=NONE guibg=NONE gui=NONE
hi rubyInstanceVariable  guifg=#006600 guibg=NONE gui=NONE
hi rubyInclude  guifg=#294277 guibg=NONE gui=NONE
hi rubyGlobalVariable  guifg=#006600 guibg=NONE gui=NONE
hi rubyRegexp  guifg=#df0700 guibg=NONE gui=NONE
hi rubyRegexpDelimiter  guifg=#df0700 guibg=NONE gui=NONE
hi rubyEscape  guifg=#33cc33 guibg=NONE gui=NONE
hi rubyControl  guifg=#294277 guibg=NONE gui=NONE
hi rubyClassVariable  guifg=NONE guibg=NONE gui=NONE
hi rubyOperator  guifg=#294277 guibg=NONE gui=NONE
hi rubyException  guifg=#294277 guibg=NONE gui=NONE
hi rubyPseudoVariable  guifg=#006600 guibg=NONE gui=NONE
hi rubyRailsUserClass  guifg=#d0533b guibg=NONE gui=NONE
hi rubyRailsARAssociationMethod  guifg=#476a97 guibg=NONE gui=NONE
hi rubyRailsARMethod  guifg=#476a97 guibg=NONE gui=NONE
hi rubyRailsRenderMethod  guifg=#476a97 guibg=NONE gui=NONE
hi rubyRailsMethod  guifg=#476a97 guibg=NONE gui=NONE
hi erubyDelimiter  guifg=NONE guibg=NONE gui=NONE
hi erubyComment  guifg=#c3741c guibg=NONE gui=NONE
hi erubyRailsMethod  guifg=#476a97 guibg=NONE gui=NONE
hi htmlTag  guifg=#294277 guibg=NONE gui=NONE
hi htmlEndTag  guifg=#294277 guibg=NONE gui=NONE
hi htmlTagName  guifg=#294277 guibg=NONE gui=NONE
hi htmlArg  guifg=#294277 guibg=NONE gui=NONE
hi htmlSpecialChar  guifg=#c5060b guibg=NONE gui=NONE
hi javaScriptFunction  guifg=#294277 guibg=NONE gui=NONE
hi javaScriptRailsFunction  guifg=#476a97 guibg=NONE gui=NONE
hi javaScriptBraces  guifg=NONE guibg=NONE gui=NONE
hi yamlKey  guifg=NONE guibg=NONE gui=NONE
hi yamlAnchor  guifg=#006600 guibg=NONE gui=NONE
hi yamlAlias  guifg=#006600 guibg=NONE gui=NONE
hi yamlDocumentHeader  guifg=NONE guibg=#ecf0e7 gui=NONE
hi cssURL  guifg=NONE guibg=NONE gui=NONE
hi cssFunctionName  guifg=#476a97 guibg=NONE gui=NONE
hi cssColor  guifg=#c5060b guibg=NONE gui=NONE
hi cssPseudoClassId  guifg=NONE guibg=NONE gui=NONE
hi cssClassName  guifg=NONE guibg=NONE gui=NONE
hi cssValueLength  guifg=#294277 guibg=NONE gui=NONE
hi cssCommonAttr  guifg=#06960e guibg=NONE gui=NONE
hi cssBraces  guifg=NONE guibg=NONE gui=NONE

