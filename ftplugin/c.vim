"
" ---------- tabulator / shiftwidth --------------------
" Set tabulator and shift width to 2
"
setlocal noexpandtab
setlocal  tabstop=8
setlocal  shiftwidth=8
"
